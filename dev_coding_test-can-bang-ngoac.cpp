#include <iostream>
#include <stack>
#include <string>

using namespace std;

bool isBalanced(const string& str) {
    stack<char> st;
    
    for (size_t i = 0; i < str.length(); ++i) {
    	char bracket = str[i];
        if (bracket == '(' || bracket == '[' || bracket == '{') {
            st.push(bracket);
        } else {
            if (st.empty()) {
                return false;
            }
            
            char top = st.top();
            st.pop();
            
            if ((bracket == ')' && top != '(') ||
                (bracket == ']' && top != '[') ||
                (bracket == '}' && top != '{')) {
                return false;
            }
        }
    }
    
    return st.empty();
}

int main() {
    int N;
    string input;
    bool validInput = false;
    
    do {
        cin >> N;
        cin.ignore();
        
        if (N >= 1 && N <= 100) {
            validInput = true;
        } else {
            cerr << "1 <= N <= 100" << endl;
        }
    } while (!validInput);
    
    string tests[N];
    
    for (int i = 0; i < N; ++i) {
        validInput = false;
        do {
            cout << "Test " << i + 1 << ": ";
            getline(cin, input);
            
            if (input.length() <= 100000) {
                bool validChars = true;
                for (size_t j = 0; j < input.length(); ++j) {
                    char c = input[j];
                    if (c != '(' && c != ')' && c != '[' && c != ']' && c != '{' && c != '}') {
                        validChars = false;
                        cerr << "Invalid string" << endl;
                        break;
                    }
                }
                if (validChars) {
                    validInput = true;
                }
            } else {
                cerr << "string < 100000" << endl;
            }
        } while (!validInput);
        
        tests[i] = input;
    }
    
    cout << "----------" << endl;
    for (int i = 0; i < N; ++i) {
        if (isBalanced(tests[i])) {
            cout << "true" << endl;
        } else {
            cout << "false" << endl;
        }
    }
    
    return 0;
}

