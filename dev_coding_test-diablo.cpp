#include <iostream>
using namespace std;

int calculateMinimumGold(int m, int d, int k, int c) {
    int initialGold = d;
    int goldNeeded = 0;
    while (m > 0) {
        if (m == 1 && d - k == 0)
            return goldNeeded;
            
        if (d <= 0)
            return -1;
            
        if (d <= k) {
            goldNeeded += c;
            d = initialGold;
        }
        d -= k;
        m--;
    }
    return goldNeeded;
}

int main() {
    int M, D, K, C;
    cin >> M >> D >> K >> C;
    
    int minGold = calculateMinimumGold(M, D, K, C);
    cout << "----------" << endl;
    if (minGold == -1)
        cout << "Not pass.";
    else
        cout << "Min gold: " << minGold << endl;
    
    return 0;
}

