#include <iostream>
using namespace std;

int factorial(int n) {
    if (n <= 1)
        return 1;
    return n * factorial(n - 1);
}

int waysToScore(int x, int y) {
    return factorial(x + y) / (factorial(x) * factorial(y));
}

int main() {
    int N;
    cin >> N;

    int arr[N][2];

    for (int i = 0; i < N; ++i) {
        int x, y;
        cin >> x >> y;
        arr[i][0] = x;
        arr[i][1] = y;
    }
    
	cout << "----------" << endl;
    for (int i = 0; i < N; ++i) {
        int x = arr[i][0];
        int y = arr[i][1];
        cout << waysToScore(x, y) << endl;
    }

    return 0;
}

